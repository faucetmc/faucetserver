﻿using System;

namespace Faucet.Protocol
{
    public class ProtocolException : Exception
    {
        public ProtocolException(string msg) :base(msg){}
    }
}