﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Faucet.Protocol.Packet;

namespace Faucet.Protocol
{
    public class DirectionData
    {
        private readonly ProtocolDirection _direction;

        public ProtocolDirection Direction => _direction;

        private Dictionary<Type,Dictionary<ProtocolVersion,byte>> Status = new Dictionary<Type, Dictionary<ProtocolVersion, byte>>();
        private Dictionary<Type, Dictionary<ProtocolVersion, byte>> Login = new Dictionary<Type, Dictionary<ProtocolVersion, byte>>();
        private Dictionary<Type, Dictionary<ProtocolVersion, byte>> Game = new Dictionary<Type, Dictionary<ProtocolVersion, byte>>();
        
        public DirectionData(ProtocolDirection direction)
        {
            _direction = direction;
        }

        public void Register<TPacket>(ProtocolPhase phase,params ProtocolMapping[] mappings)
        {
            // if handshake don't do anything because handshake only has the ONE packet id so we don't even store it.
            if (phase == ProtocolPhase.Handshake || typeof(TPacket) == typeof(HandshakePacket))
                return;
            
            var type = typeof(TPacket);
            
            // I know I know, its kinda ugly,
            // But PRs are always open :)
            var phaseDict = phase switch
            {
                ProtocolPhase.Status => Status,
                ProtocolPhase.Login => Login,
                ProtocolPhase.Game => Game,
                // Handshake isn't in this switch because its picked up earlier, 
                // If for some reason Handshake makes it this far we throw an exception.
                _ => throw new InvalidEnumArgumentException($"Can't use this phase for Register. {Enum.GetName(typeof(ProtocolPhase), phase)}" )
            };

            if (phaseDict.ContainsKey(type))
                throw new ProtocolException($"Cannot insert mapping for {type.FullName} in phase {Enum.GetName(typeof(ProtocolPhase),phase)}. Mapping exists.");

            var typeDict = new Dictionary<ProtocolVersion, byte>();

            foreach (var mapping in mappings)
            {
                if(typeDict.ContainsKey(mapping.Version))
                    throw new ArgumentException($"Provided mappings has duplicate value!", nameof(mapping));
                
                typeDict.Add(mapping.Version,mapping.Id);
            }
            
            // We don't check if its already added because we do that earlier.
            phaseDict.Add(type, typeDict);

        }

        public byte GetId<TPacket>(ProtocolVersion version, ProtocolPhase phase) where TPacket : IPacket, new()
        {
            // if handshake just return 0x00 because handshake only has the ONE packet id so we don't even store it.
            if (phase == ProtocolPhase.Handshake || typeof(TPacket) == typeof(HandshakePacket))
                return 0x00;

            var phaseDict = phase switch
            {
                ProtocolPhase.Status => Status,
                ProtocolPhase.Login => Login,
                ProtocolPhase.Game => Game,
                // Handshake isn't in this switch because its picked up earlier, 
                // If for some reason Handshake makes it this far we throw an exception.
                _ => throw new InvalidEnumArgumentException($"Can't find phase {Enum.GetName(typeof(ProtocolPhase), phase)}" )
            };

            
            var type = typeof(TPacket);

            if (!phaseDict.ContainsKey(type))
                throw new ProtocolException($"Can't get Id for {type.FullName} from {Enum.GetName(typeof(ProtocolPhase),phase)}");

            var typeDict = phaseDict[type];
            
            // we now have a dictionary with all versions where an ID changes, we need to find which version the given
            // Id matches up with in terms of compat. Which would mean we go Price is Right rules, Closest without going over

            byte id;
            
            if (typeDict.TryGetValue((ProtocolVersion) version, out id))
                return id;
            
            // Packet doesn't have direct version translation, We need to find the right one.
            version = typeDict.Keys.First(ver => ver <= version);
            if(!typeDict.TryGetValue(version, out id))
                throw new ProtocolException($"Can't find packet Id for {type.FullName}");

            return id;
        }
    }
}