﻿using System;
using System.IO;
using System.Text;
using Faucet.Protocol.Packet;

namespace Faucet.Protocol
{
    public static class ProtocolUtil
    {
        
        private static readonly DirectionData ToServer = new DirectionData(ProtocolDirection.ToServer);
        private static readonly DirectionData ToClient = new DirectionData(ProtocolDirection.ToClient);
        
        static ProtocolUtil()
        {
            #region Handshake
            ToServer.Register<HandshakePacket>(ProtocolPhase.Handshake,
                new ProtocolMapping(ProtocolVersion.Minecraft_1_8, 0x00));
            #endregion

            #region Status
            ToClient.Register<StatusResponsePacket>(ProtocolPhase.Status,
                new ProtocolMapping(ProtocolVersion.Minecraft_1_8, 0x00));
            
            ToClient.Register<StatusPingPongPacket>(ProtocolPhase.Status,
                new ProtocolMapping(ProtocolVersion.Minecraft_1_8, 0x01));
            
            ToServer.Register<StatusRequestPacket>(ProtocolPhase.Status,
                new ProtocolMapping(ProtocolVersion.Minecraft_1_8, 0x00));

            ToServer.Register<StatusPingPongPacket>(ProtocolPhase.Status,
                new ProtocolMapping(ProtocolVersion.Minecraft_1_8, 0x01));
            #endregion

            #region Login

            // TODO: Add Login packets and register them

            #endregion

            #region Game

            // TODO: Add Game packets and register them.

            #endregion
            
        }

        public static byte GetId<TPacket>(ProtocolVersion version, ProtocolDirection direction, ProtocolPhase phase = ProtocolPhase.Game)
            where TPacket : IPacket, new() => GetId<TPacket>((int) version, direction, phase);

        /// <summary>
        /// Grabs the packet ID for a specified version of the protocol
        /// </summary>
        /// <param name="version">Protocol version from the client</param>
        /// <param name="direction">Direction the packet is traveling</param>
        /// <param name="phase">The phase in which to grab ID from</param>
        /// <typeparam name="TPacket">Packet to get the version for</typeparam>
        /// <returns></returns>
        public static byte GetId<TPacket>(int version, ProtocolDirection direction, ProtocolPhase phase = ProtocolPhase.Game) where TPacket : IPacket, new()
        {
            return (direction == ProtocolDirection.ToServer 
                    ? ToServer
                    : ToClient)
                .GetId<TPacket>((ProtocolVersion)version, phase);
        }

        #region Readers

        public static string ReadString(Stream stream)
        {
            var length = ReadVarInt(stream);
            if(length > short.MaxValue)
                throw new ProtocolException($"Cannot read string longer than short.MaxValue (got {length} characters)");

            var buf = new byte[length];
            stream.Read(buf, 0, length);

            return Encoding.UTF8.GetString(buf);
        }

        public static byte[] ReadArray(Stream stream) => ReadArray(stream, stream.Length);

        public static byte[] ReadArray(Stream stream, long limit)
        {
            var length = ReadVarInt(stream);
            if(length > limit)
                throw new ProtocolException($"Cannot read array longer than {limit} (got {length} bytes)");

            var buf = new byte[length];
            stream.Read(buf, 0, length);
            
            return buf;
        }

        public static int[] ReadVarIntArray(Stream stream)
        {
            var length = ReadVarInt(stream);
            var buf = new int[length];

            for (var i = 0; i < length; i++)
            {
                buf[i] = ReadVarInt(stream);
            }

            return buf;
        }

        public static string[] ReadStringArray(Stream stream)
        {
            var length = ReadVarInt(stream);
            var buf = new string[length];
            for (var i = 0; i < length; i++)
            {
                buf[i] = ReadString(stream);
            }

            return buf;
        }

        public static int ReadVarInt(Stream stream)
        {
            var output = 0;
            var num = 0;
            byte[] input = new byte[1];
            do
            {
                Array.Clear(input,0,1);
                stream.Read(input,0,1);

                int value = (input[0] & 0b01111111);
                output |= (value << (7 * num));
                num++;
                
                if(num > 5) 
                    throw new ProtocolException($"VarInt too big");


            } while ((input[0] & 0b10000000) != 0);

            return output;
        }

        public static short ReadVarShort(Stream stream)
        {
            byte[] buf = new byte[2];

            stream.Read(buf, 0, 2);

            return BitConverter.ToInt16(new byte[2] {buf[1], buf[0]});
        }

        public static long ReadVarLong(Stream stream)
        {
            var num = 0;
            var result = 0L;
            var read = new byte[1];
            do
            {
                Array.Clear(read,0,1);
                stream.Read(read, 0, 1);

                var value = (read[0] & 0b01111111);
                result |= (value << (7 * num));
                num++;
                if(num > 10)
                    throw new ProtocolException("VarLong is too big!");
            } while ((read[0] & 0b10000000) != 0);

            return result;
        }
        
        public static string ReadUuid(Stream stream) => "";

        #endregion

        #region Writers

        public static void Write(string toWrite, Stream stream)
        {
            if(toWrite.Length > short.MaxValue)
                throw new ProtocolException($"Cannot write string longer than short.MaxValue (got {toWrite.Length} characters)");

            byte[] buf = Encoding.UTF8.GetBytes(toWrite);
            
            Write(buf.Length, stream);
            stream.Write(buf);
        }

        public static void Write(byte[] array, Stream stream)
        {
            if(array.Length > short.MaxValue)
                throw new ProtocolException($"Cannot send byte array longer than short.MaxValue (got {array.Length} bytes)");
            
            Write(array.Length, stream);
            stream.Write(array);
        }

        public static void Write(string[] toWrite, Stream stream)
        {
            Write(toWrite.Length, stream);
            foreach (var str in toWrite)
            {
                Write(str, stream);
            }
        }

        public static void Write(int value, Stream stream)
        {
            do
            {
                byte temp = (byte) (value & 0b01111111);
                // C# has unsigned types so we don't have a >>> operator
                // We do some casting stuff to make up for that
                value = (int)((uint)value >> 7);
                
                if (value != 0)
                    temp |= 0b10000000;

                stream.Write(new [] {temp});
            } while (value != 0);
        }

        public static void Write(short value, Stream stream)
        {

            var buf = BitConverter.GetBytes(value);
            buf = new byte[2] {buf[1], buf[0]};
            
            stream.Write(buf);
        }

        public static void Write(long value, Stream stream)
        {
            do
            {
                byte temp = (byte) (value & 0b01111111);
                value = (long) ((ulong) value >> 7);
                
                if (value != 0)
                    temp |= 0b10000000;
                
                stream.Write(new []{temp});
            } while (value != 0);
        }

        public static void WriteUuid(string uuid, Stream stream)
        {
            throw new NotImplementedException("Cannot write Uuid!");
        }

    #endregion
    }
}