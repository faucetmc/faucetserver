﻿using System.IO;
using static Faucet.Protocol.ProtocolUtil;

namespace Faucet.Protocol.Packet
{
    public class HandshakePacket: BasePacket
    {
        public int ProtocolVersion { get; private set; }
        public string ServerAddress { get; private set; }
        public int ServerPort { get; private set; }
        public int NextState { get; private set; }
        
        public override void Read(Stream stream)
        {
            ProtocolVersion = ReadVarInt(stream);
            ServerAddress = ReadString(stream);
            ServerPort = ReadVarShort(stream);
            NextState = ReadVarInt(stream);
        }
        
        public override byte[] Write()
        {
            throw new System.NotImplementedException();
        }
    }
}