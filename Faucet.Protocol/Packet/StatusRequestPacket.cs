﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Faucet.Protocol.Packet
{
    public class StatusRequestPacket : BasePacket
    {
        public override byte[] Write()
        {
            return new byte[0];
        }

        public override void Read(Stream stream) {}
    }
}