﻿using System.IO;
using System.Threading.Tasks;

namespace Faucet.Protocol.Packet
{
    public class StatusResponsePacket : BasePacket
    {
        public static readonly string RESPONSE = "{\"version\":{\"name\":\"1.15.2\", \"protocol\":578}, \"players\": {\"max\": 69420, \"online\": 42069, \"sample\":[]}, \"description\":{\"text\":\"Faucet Development Status Message!\"}, \"favicon\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAADUlEQVR42mMUfvLyEwAFxgLUEQDTDwAAAABJRU5ErkJggg==\"}";

        public override byte[] Write()
        {
            var stream = new MemoryStream();
            ProtocolUtil.Write(RESPONSE, stream);
            return stream.ToArray();
        }

        public override void Read(Stream stream)
        {
            throw new System.NotImplementedException();
        }
    }
}