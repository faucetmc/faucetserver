﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Faucet.Protocol.Packet
{
    public abstract class BasePacket: IPacket
    {
        public abstract byte[] Write();
        public abstract void Read(Stream stream);

        public byte[] Wrap()
        {
            var write = Write();
            var output = new byte[write.Length+1];
            // TODO: Grab packet ID 
            
            write.CopyTo(output,1);
            return output;
        }
    }
}