﻿using System;
using System.IO;
using System.Threading.Tasks;

namespace Faucet.Protocol.Packet
{
    public class StatusPingPongPacket : BasePacket
    {
        public long Payload;

        public override byte[] Write()
        { 
            return BitConverter.GetBytes(Payload);
        }

        public override void Read(Stream stream)
        {
            byte[] buf = new byte[8];
            stream.Read(buf, 0, 8);

            Payload = BitConverter.ToInt64(buf);
        }
    }
}