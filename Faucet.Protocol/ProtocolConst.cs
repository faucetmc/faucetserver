﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Faucet.Protocol
{
    public static class ProtocolConst
    {
        public const int MaxPacketId = 0xFF;

    }

    public enum ProtocolDirection
    {
        ToClient,
        ToServer
    }

    public enum ProtocolPhase
    {
        Handshake = -1,
        Game = 0,
        Status = 1,
        Login = 2
    }

    
    public enum ProtocolVersion: int
    {
        Unknown          = -1,
        Minecraft_1_8    = 47,
        Minecraft_1_9    = 107,
        Minecraft_1_9_1  = 108,
        Minecraft_1_9_2  = 109,
        Minecraft_1_9_4  = 110,
        Minecraft_1_10   = 210,
        Minecraft_1_11   = 315,
        Minecraft_1_11_1 = 316,
        Minecraft_1_12   = 335,
        Minecraft_1_12_1 = 338,
        Minecraft_1_12_2 = 340,
        Minecraft_1_13   = 393,
        Minecraft_1_13_1 = 401,
        Minecraft_1_13_2 = 404,
        Minecraft_1_14   = 477,
        Minecraft_1_14_1 = 480,
        Minecraft_1_14_2 = 485,
        Minecraft_1_14_3 = 490,
        Minecraft_1_14_4 = 498,
        Minecraft_1_15   = 573,
        Minecraft_1_15_1 = 575,
        Minecraft_1_15_2 = 578,
    }
}