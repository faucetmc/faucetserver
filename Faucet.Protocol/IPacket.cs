﻿using System.IO;
using System.Threading.Tasks;

namespace Faucet.Protocol
{
    public interface IPacket
    {
        public void Read(Stream data);
        public byte[] Write();
    }
}