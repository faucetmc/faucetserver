﻿namespace Faucet.Protocol
{
    public readonly struct ProtocolMapping
    {
        public readonly byte Id;
        public readonly ProtocolVersion Version;

        public ProtocolMapping(ProtocolVersion version, byte id)
        {
            Version = version;
            Id = id;
        }
    }
}