﻿using System;
using System.IO;
using System.Threading.Tasks;
using Serilog;

namespace Faucet.Server
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .WriteTo.Console()
                .MinimumLevel.Debug()
                .CreateLogger();
            
            Task.Delay(-1).GetAwaiter().GetResult();
        }
    }
}